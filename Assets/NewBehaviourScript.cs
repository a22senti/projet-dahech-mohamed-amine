using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


public class NewBehaviourScript : MonoBehaviour
{
	
	 public float speed = 3.0f ;
	 public InputAction activateRotation ;
	 public bool check=false ;
	 
	 
	// Start is called before the first frame update
	private void Start()
	{
		activateRotation.Enable();
		activateRotation.started += ctx => check=!check;
    	//activateRotation.canceled += ctx => OnRotationCanceled() ;
	}
	//void OnRotationStarted()
//{
	//check=true ;

    // Code à exécuter lorsque l'action est démarrée (touche "R" appuyée)
    //Debug.Log("Rotation started");
//}

//void OnRotationCanceled()
//{
    // Code à exécuter lorsque l'action est annulée (touche "R" relâchée)
   // Debug.Log("Rotation canceled");
	//check=false;
//}
 
	// Update is called once per frame
	 void Update()
	{
		if (check==true) 
		{

		transform.Rotate(Vector3.up * speed * Time.deltaTime);
		}
	
		

	}
}



