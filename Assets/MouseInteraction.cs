using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseInteraction : MonoBehaviour,IPointerClickHandler, IPointerEnterHandler,IPointerExitHandler
{
    private Rigidbody rigidbody;
    private Renderer rend ;
    private Color couleurdebut ;
        void Start()
    {
        rigidbody=GetComponent<Rigidbody>() ;
        rend=GetComponent<Renderer>() ;
        couleurdebut=rend.material.color ;

        
    }
    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
            rigidbody.AddForce(Camera.main.transform.forward*1000f,ForceMode.Force) ;
    }
    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
    {
        rend.material.color=Color.red ;
    }

    void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
    {
        rend.material.color=couleurdebut ;
    }

}
